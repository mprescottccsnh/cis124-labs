//Global variables and functions
var redColorVal, greenColorVal, blueColorVal, rgb;

function getRandomColorVal() {
    //Math.random() generates a floating point number between 0 and 1
    //We want red, green and blue values between 0 and 255  
    redColorVal = Math.floor(Math.random() * 256);;
    greenColorVal = Math.floor(Math.random() * 256);;
    blueColorVal = Math.floor(Math.random() * 256);;
    return "rgb(" + redColorVal + "," + greenColorVal + "," + blueColorVal + ")";
}

function getInputColorVal() {
    //Get the string typed in by the user in the input box
    rgb = $("#colorValue").val();
    return "rgb(" + rgb + ")";
}

$(document).ready(function(){
    
    $("#random").click(function(){
        $("#colorBlock").css("background", getRandomColorVal());
    });
    
    $("#update").click(function() {
        $("#colorBlock").css("background", getInputColorVal());
    });
});

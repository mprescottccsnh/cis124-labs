//jQuery Button Demo

$(document).ready(function () {

    var monkeyDiv = $("#wiseMonkey");
    var monkeyPic;
    
    $('button').each(function () {        
        $(this).click(function () {
            monkeyPic = $(this).find("img").attr("src");
            monkeyDiv.css("background-image","url(" + monkeyPic + ")");
        });
    });
    
});
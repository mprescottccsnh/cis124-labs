
$(document).ready(function () {
    
    $('#stop').toggle(
        function () {
            $("#song").trigger('pause');
            $(this).html("Start the music!");
        },
        function () {
            $("#song").trigger('play');
            $(this).html("Stop the music!");
        }
    );
    
});

